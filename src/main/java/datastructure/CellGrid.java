package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;


    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;

        grid = new CellState[rows][columns];

        for (int r=0; r<rows; r++) {
            for(int c=0; c<columns; c++){
                grid[r][c] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row <= 0 && row > numRows()){
            throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ row);
        }
        if(column <= 0 && column > numColumns()){
            throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ column);
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row <= 0 && row > numRows()){
            throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ row);
        }
        if(column <= 0 && column > numColumns()){
            throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ column);
        }

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);

        for (int r=0; r<rows; r++){
            for(int c=0; c<columns; c++){
                gridCopy.set(r,c,grid[r][c]);
            }
        }
        return gridCopy;
    }
    
}
